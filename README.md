# Ateliers accessibilité animés par le Pôle internet de la DILA

![ateliers.png](https://bitbucket.org/repo/K66MnA/images/1529748729-ateliers.png)

## Composants html/js + ARIA conformes au RGAA3

Espace d'élaboration de composants accessibles ou d'exercices pour des ateliers. Une fois le travail bien avancé (et même, idéalement, finis), les composants sont versés dans le dépôt [capidila](https://bitbucket.org/pidila/capidila) et leur démo publique est visible sur [le site du pidila](http://pidila.bitbucket.org/capidila/).

## Contributions

Vous pouvez réclamer un composant ou un exercice en ouvrant un ticket :-)